**Índice**  

 [[_TOC_]]  
  
  
# Subservicios

Los subservicios serán agrupaciones temáticas. No existe una metodología concreta a la hora de crear nuevos subservicios. Ante la llegada de un proyecto que tenga que integrarse en plataforma existen tres opciones:
1. Todo el proyecto en un subservicio propio
2. Reusar uno de los existentes genérico: trafico, medioambiente,aguas,...
3. Clonar para el proyecto los subservicios genéricos necesarios. Por ejemplo: trafico_smge, medioambiente_emt

Lo más rápido/sencillo, pero más desorganizado, la (1)
Lo más adecuado pero más costoso de mantener: la (2)
Un equilibrio, la (3), pero nos da un poco de más trabajo que la (1) y la (2)

- Si es un proyecto que va a meter ítems en plataforma de forma masiva 
    - y no tenemos claro que vaya a ser muy útil en el futuro -> (1)
    - y vemos que es un proyecto que va a ser útil
        - Si hay ámbitos temáticos donde se aportan pocos ítems (2-3 sensores), poner en el subservicio genérico (medioambiente, tráfico, etc). Es decir opción (2). Así hemos hecho cuando se han incorporado 3 estaciones de AEMET.
        - Si hay ámbitos temáticos donde se aportan muchos ítems (10-15 grupos, tipos de entidades, suscripciones…). Es decir, si tiene una cierta entidad, poner la coletilla _proyecto, es decir opción (3)


En todo caso, <ins>se ha decidido consensuar con el Ayuntamiento la estrategia adecuada<ins>.

## Nombre Público

Al crear un subservicio, en el campo "Nombre Público" se pondrá a continuación del nombre del subservicio y entre paréntesis 3 caracteres (únicos entre el resto de subservicios existentes; es decir no se pueden tener las mismas 3 letras, para subservicios diferentes) que es el que se usará en [las apikey de ese subservicio](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/iotagents/README.md#apikeys).

![ejemplo en el subservicio de tráfico](images/image.png)

## Entornos de prueba: INT, PRE

En entornos de prueba (int y pre), crearemos subservicios por proyecto. Así por ejemplo si viene un proyecto con sensores medioambientales de calidad medio/baja, la idea es:
- que vayan en PROD al subservicio mediambiente_movilidad
- En INT y PRE crearíamos subservicios específicos: mediambiente_movilidad_proyectoX

De esta manera se podrán hacer revisiones de forma más sencilla.


# Usuarios.

Existen dos tipos de usuarios:
- Los asignados a personas físicas
- Los asignados a sistemas informáticos / sensores / ...

En la *descripción de los usuarios* se seguirá la siguiente nomenclatura: 
- Para personas físicas: [TIPO] - [EQUIPO] - [SUBEQUIPO] - [DESCRIPCION]
- Para personas físicas: [TIPO] - [PROYECTO] - [DESCRIPCION]
Donde cada uno de los campos entre corchetes tiene un significado que se explica a continuación.
	
**Tipo**
- USER, para personas físicas
- SOFTWARE, para sistemas informáticos

**Equipo**
- OTP, para el equipo del proyecto VLCi
- Plataforma VLCi, para el equipo de la Plataforma VLCi
- Mto Impulso, para el equipo de que mantiene los proyectos de Impulso

**Subequipo (OPCIONAL)**
- Desarrollo, para el equipo de Integración de la OTP (proyecto VLCi)

**Proyecto**
- Impulso
- Conecta
- etc

Ejemplos:
- USER - OTP - Desarrollo - Julia Villanueva
- USER - Mto Impulso - Andrés Castelló
- SOFTWARE - Impulso C5 - residuos admin