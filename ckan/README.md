**Índice**  

 [[_TOC_]]  
  
  
# Datasets CKAN

Todo dataset que se creen en CKAN debe cumplimentar los campos adecuadamente. En concreto:
- `Descripción`: Los datasets de CKAN deben describir correctamente los diferentes recursos que publican. Además, de cada recurso, debe haber una explicación detallada de los datos que se publican (columnas) y su significado. Por ejemplo, si un recurso tiene 4 columnas de datos: id, cod, period, kpi. Se tendrán que listar esas columnas y explicar qué es cada una de ellas para que la persona/sistema que consuma esos datos sepa qué signfican.

# Recursos CKAN

Todo recurso asociado a un dataset que se cree en CKAN debe cumplimentar los campos adecuadamente. En concreto:
- `Nombre`: El nombre del recurso debe coincidir (en minúsculas) con el ID de la entidad KPI.
- `Descripción`: Los recursos de un dataset en CKAN deben describir correctamente lo que publican. Además de una descripción general, debe haber una explicación detallada de los datos que se publican (columnas) y su significado. Por ejemplo, si un recurso tiene 4 columnas de datos: id, cod, period, kpi. Se tendrán que listar esas columnas y explicar qué es cada una de ellas para que la persona/sistema que consuma esos datos sepa qué signfican.

