**Índice**  

 [[_TOC_]]  
  
  
# Nomenclatura. 

## Protocolos o Grupos de Dispositivos o Services
Todas los protocolos (o también llamados grupos de dispositivos o services) deben tener el siguiente texto en el campo Descripción: [Equipo] - [Proyecto] - [Origen] - [Medición] - [Descripción extra si es necesaria]

Donde cada uno de los campos entre corchetes tiene un significado que se explica a continuación.
	
**Equipo**
- OTP, para el equipo del proyecto VLCi
- El nombre de la empresa que realice la suscripción
    
**Proyecto**
- VLCI, para suscripciones realizadas por la OTP dentro del proyecto VLCi.
- Otros ejemplos: Impulso C7, Connecta

**Origen**
Indica qué componente tecnológico es el que genera el dato que llega a la plataforma. Ejemplos: 
- ETL XXX
- Sensor Parking
- Camión de bomberos

**Ejemplos**
- HOPU - Impulso C10 - Sensor Ruido - Ruido diario
- OTP - VLCI - ETL Tráfico Realtime - Estado del tráfico
- OTP - VLCI - Sensor Parking - Plazas Libres


## APIKEYs
Los tokens que se generen en el IoT Agent deben ser del tipo: [vlc][entorno][subservicio][token]
Donde cada uno de los campos entre corchetes tiene un significado que se explica a continuación.

- vlc: es una constante que identifica a valencia
- entorno: es uno de estos valores posibles: int, pre, pro
- subservicio: son los 3 caracteres especiales que se han definido para [cada subservicio](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/tree/main/admin?ref_type=heads#nombre-p%C3%BAblico) ![Ejemplo subservicio de tráfico](images/image.png)
- token: una combinación aleatoria de 16 characters. Sin ningún carácter especial. Carácteres válidos: a-z y 0-9 únicamente

**Ejemplo de token**
vlcproaguuqf62ja76bceqot5
- entorno: pro (producción)
- subservicio: agu -> Del subservicio aguas.
- token: uqf62ja76bceqot5


## Identificador Devices
El valor a darle a los IDs de los Devices (aplica lo mismo para los IDs de las entidades de Context Broker) debe ser: [Proyecto]_[Identificador único]
Donde cada uno de los campos entre corchetes tiene un significado que se explica a continuación.

**Proyecto**
Texto corto que identifica el proyecto donde se enmarca ese dispositivo.
	
**Ejemplos**
- Un sensor de Hopu del C10 de Impulso: ImpulsoC10_2766555
- Un sensor de Contenedores de Aceite del proyecto de la Diputación de 2023: CADIPU2023_NB1228182121

__IMPORTANTE__ Tanto para el campo [Proyecto] como el campo [Identificador único] deben estar formado únicamente por números [0-9] o letras [a-zA-z] sin tildes ni caracteres especiales como la ñ o ç. En caso de necesitar utilizar separadores se debe utilizar la barra baja _ (aunque no se recomienda su uso). En ningún caso se pueden utilizar los dos puntos : ni otros signos de puntuación. Otras restricciones que aplican a estos y otros elementos se pueden consultar en:
https://github.com/telefonicaid/fiware-orion/blob/master/doc/manuals/orion-api.md#identifiers-syntax-restrictions


# Buenas prácticas con los Grupos de Dispositivos

- El siguiente atributo se debe configurar como se explica, para explicitar que se usará el formato abierto JSON como punto de entrada al Agente IoT. 
    - Si se usa Postman para crear el grupo de dispositivos. poner "payloadType": "IotaJson" a la altura de "description", "apikey", etc.
    - Si se usa el Portal IoT se añadirá en la caja "Tipo De Payload En La Medida", seleccionando la opcion "IotaJson".

 ![Payload](images/imagenpayload.PNG)
 

- El atributo timestamp o Marca de Tiempo se debe configurar a false y se añadirá una entrada a la lista de atributos tal y como se explica a continuación. 
    - Si se usa Postman para crear el grupo de dispositivos: 
        - Poner "timestamp": false, a la altura de "description", "apikey", etc.
        - En el array de Attributes, añadir la siguiente linea: { "object_id": "TimeInstant", "name": "TimeInstant", "type": "DateTime",  "expression": "0|now|toisodate"}
    - Si se usa el Portal IoT 
        - Poner a false el atributo "Marca de Tiempo".
        - En la sección de mapeo de atributos, añadir un Atributo TimeInstant de tipo DateTime con la expresión: 0|now|toisodate

 ![TimeInstant](images/imagentimeinstant.png)

- Permitir valores nulos para todos aquellos atributos que sean de tipo Number.
    - Si se usar Postman para crear el grupo de dispositivos, poner "skipValue": "permitirnull"
    - Si se usar el Portal IoT, en "Valor para permitir medida" añadir "permitirnull"

![alt text](images/imagenPermirNull.png)


# Tipos de datos

La Plataforma VLCi permite definir el tipo de los datos en varios de sus componentes. Para un uso homogéneo de los tipos de datos en todas las integraciones, en la siguiente tabla se especifícan los tipos a utilizar.

|Tipo de Dato|Dispositivos IoT / Entidades ContextBroker|Ejemplo|
|---|---|---|
|Cadenas de texto|Text|Ejemplo de cadena de texto|
|Números enteros|Number|23|
|Números decimales|Number|4278.02|
|Fechas|DateTime|2022-06-21T11:34:16Z|
|Localización|geo:json|Ver ejemplo más abajo|
|Booleanos|Boolean|true/false|
|Arrays|StructuredValue|["Valencia", "Alicante", "Castellón"]|
|JSON|StructuredValue|{"type":"PostalAddress"}|


**NOTA Importante**

Al GIS municipal no le gustan los atributos de tipo StructuredValue. El GIS prefiere atributos tipo Text, con cada valor del StructuredValue separado por comas. Así por ejemplo en lugar de un atributo StructuredValue con el valor: [Caravan_Mains_Socket, CHAdeMO, CCS/SAE, Dual_CHAdeMO], el atributo Text tendría el siguiente valor: "Caravan_Mains_Socket, CHAdeMO, CCS/SAE, Dual_CHAdeMO". Podría ser conveniente tener el atributo original como StructuredValue y un atributo secundario con el tipo Text y sólo usado en las suscripciones al GIS municipal.


## Atributos numéricos
Todos los atributos numéricos deben seguir el formato ####.##

Es decir, no usar separadores de miles y utilizar el punto como separador decimal

## Atributos de fecha
**Fecha** - los atributos date que estrictamente se refieren a una fecha sin hora se deben formatear como: AAAA-MM-DD. Ejemplo: 2022-06-20 (que se corresponde al 20 de junio de 2022)

**Timestamp** 
  - Los atributos que representan un timestamp seguiran el formato ISO 8601. Este formato acepta tanto un formato de hora UTC como un formato de hora local.
    - Ejemplo de timestamp con hora UTC: 2022-06-21T11:34:16Z (que se corresponde al 21 de junio de 2022, a las 11 horas, 34 minutos, 16 segundos en GMT+0)
    - Ejemplo de timestamp con hora Local (verano) Europe/Madrid: 2024-10-24T09:46:00+02:00 (que se corresponde al 24 de octubre de 2024, a las 9 horas, 46 minutos, 0 segundos en GMT+2)
    - Ejemplo de timestamp con hora Local (invierno) Europe/Madrid: 2024-12-02T17:03:00+01:00 (que se corresponde al 2 de diciembre de 2024, a las 17 horas, 3 minutos, 0 segundos en GMT+1)

## Location
En el context broker, la location será siempre de tipo geo:json. 

Ejemplo:

```
  "location": {
    "value": {
      "type": "Point",
      "coordinates": [-0.322502, 39.464473]
    },
    "type": "geo:json"
  }
```  

## Location - proyección de coordenadas
El sistema de proyección a utilizar será el estándar de los GeoJSON: WGS84 (GeoJSON uses a geographic coordinate reference system, World Geodetic System 1984, and units of decimal degrees. Ver: https://www.rfc-editor.org/rfc/rfc7946)


## Valores no permitidos
Por temas de seguridad, no está permitido el uso de ninguno de los siguientes caracteres en los valores de cualquier atributo:
* <
* \>
* "
* '
* =
* ;
* (
* )

Cualquier intento de uso de alguno de estos caracteres dará como resultado un error HTTP 400 Bad Request response 

[Más info, aquí](https://fiware-orion.readthedocs.io/en/3.11.0/orion-api.html#general-syntax-restrictions)

Aún así, si se quiere hacer uso de esos caracteres prohibidos con el riesgo que conlleva que puedan inyectar código fraudulento, existe un tipo de dato especial en ContextBroker que permite el uso de esos caracteres. Está documentado en el siguiente link: [Tipos de datos especiales](https://github.com/telefonicaid/fiware-orion/blob/master/doc/manuals/orion-api.md#special-attribute-types)