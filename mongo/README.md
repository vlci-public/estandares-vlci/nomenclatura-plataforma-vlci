**Índice**  

 [[_TOC_]]  
  
  
# Metodología. 

## Metodología para optimizar el espacio en disco (viene de Telefónica)
En este apartado se explica la metodología a seguir para guardar datos en el mongo de plataforma a partir de las entidades del context broker de tal manera que se optimice el espacio disponible en mongo.
El objetivo es mejorar y racionalizar la información que se guarda y que facilite en el futuro el análisis de datos al mismo tiempo que se realiza un uso controlado de los recursos. 
Las entidades en general tendrán 3 tipos de atributos:
- Campos que se consideran importantes
- Campos de importancia baja
- Campos cuyos valores no se consideran relevante guardar
Por ejemplo, se quiere evitar la situación actual donde se ha guardado millones de veces en todos los registros el mismo dato del email de la empresa de mantenimiento.

Para cada tipo de entidad se propone guardar datos mediante dos suscripciones:
- Suscripción Principal: Guarda los atributos seleccionados creando registros que tienen siempre todos los atributos seleccionados, independientemente de cuál haya sido el atributo modificado. Esto generará registros que tendrán siempre los mismos atributos rellenos.
- Suscripción Secundaria: Guarda los atributos seleccionados solo cuando cambien. Esto generará registros que tendrán únicamente los atributos modificados en cada actualización de datos realizada.

En ambas suscripciones se usarán los Headers del post del ContextBroker para establecer la bbdd mongo y el nombre de la colección:
- Fiware-ServicePath: Se indica el nombre de la colección de mongo donde se van a guardar los datos. Si se especifica una inexistente, se creará.
- fiware-service: Se indica el entorno de mongo donde se guardarán los datos (sc_vlci_pre para PRE o sc_vlci para PROD).


### Ejemplo de suscripción principal para el entorno de PRE (Con expiración)
```
{
    "description": "MongoDB - Vehicle - bomberos.+ - Camiones de bomberos sensorizados por Alkime. Proyecto SPMA - Campos Principales",
    "expires": "2023-06-30T00:00:00.000Z",
    "status": "active",
    "subject": {
        "entities": [
            {
                "idPattern": "bomberos.+",
                "type": "Vehicle"
            }
        ],
        "condition": {
            "attrs": ["calculatedTimestamp","speed","heading","location","serviceStatus","operationalStatus","inactive"], 
"notifyOnMetadataChange": false

        }
    },
    "notification": {
        "attrs": [
            "alternateName",
            "fleetVehicleId",
            "dateObserved",
            "calculatedTimestamp",
            "speed",
            "heading",
            "location",
            "measurementComments",
            "serviceStatus",
            "operationalStatus",
            "inactive"
        ],
        "onlyChangedAttrs": false,
        "attrsFormat": "normalized",
        "httpCustom": {
            "url": "http://pro-core-smc-cygnus-vlci:5066/notify",
            "headers": {
                "Fiware-ServicePath": "/bomberos_vehicles",
                "fiware-service": "sc_vlci_pre"
            }
        }
    }
}
```

### Ejemplo de suscripción secundaria para el entorno de PRE (Con expiración)
NOTA: Debemos especificar los mismos campos en la condición que en los de notificación para que funcione, además deberemos poner el atributo notifyOnMetadataChange en false.

```
{
    "description": "MongoDB - Vehicle - bomberos.+ - Camiones de bomberos sensorizados por Alkime. Proyecto SPMA - Campos Secundarios",
    "expires": "2023-06-30T00:00:00.000Z",
    "status": "active",
    "subject": {
        "entities": [
            {
                "idPattern": "bomberos.+",
                "type": "Vehicle"
            }
        ],
        "condition": {
            "attrs": ["dataProvider",
            "maintenanceOwner",
            "maintenanceOwnerEmail",
            "serviceOwner",
            "serviceOwnerEmail"
] ,
            "notifyOnMetadataChange": false

        }
    },
    "notification": {
        "attrs": [
            "dataProvider",
            "maintenanceOwner",
            "maintenanceOwnerEmail",
            "serviceOwner",
            "serviceOwnerEmail"
        ],
        "onlyChangedAttrs": true,
        "attrsFormat": "normalized",
        "httpCustom": {
            "url": "http://pro-core-smc-cygnus-vlci:5066/notify",
            "headers": {
                "Fiware-ServicePath": "/bomberos_vehicles",
                "fiware-service": "sc_vlci_pre"
            }
        }
    }
}
```


## Metodología para agrupar datos en colecciones
Las colecciones deben tener datos de la misma tipología. Así pues datos con diferentes modelos de datos o periodicidades deben ir a colecciones separadas.
En caso de ser datos muy masivos también se recomienda dividr entre proyectos/empresas las colecciones, para tener colecciones más manejables.

# Nomenclatura. 

## Nombrado de las colecciones
Las colecciones de mongo seguirán la siguiente norma para su nombrado: [Subservicio]\_[EntityType]\_[Proyecto]\_[Empresa]\_[Descripción]\_[Periodicidad]\_[Secundaria]

**IMPORTANTE**:
- El nombre de la colección será todo en minúsculas.
- El tamaño total del nombre de la colección no puede exceder los 50 caracteres.

Donde cada uno de los campos entre corchetes tiene un significado que se explica a continuación.

	
**Subservicio**
- Campo obligatorio. El nombre del subservicio en plataforma
- Ejemplos: sonometros, trafico

**EntityType**
- Campo obligatorio. El tipo de la entidad que se almacena en esta colección. No se pueden tener colecciones con tipos de entidades diferentes
- Ejemplos: noiselevelobserved, airqualityobserved

**Proyecto**
- Campo obligatorio. vlci, para colecciones realizadas por la OTP dentro del proyecto VLCi.
- Ejemplos: impulso, connecta

**Empresa**
- Campo opcional. El nombre de la empresa que instaló los sensores
- Ejemplos: bsg, hopu
    
**Descripción**
- Campo opcional. En caso de necesitar un texto para identificar mejor la colección iría aquí.
- Ejemplos: zas

**Periodicidad**
- Campo opcional. La periodicidad del dato. Si la periodicidad no es fija, se pondrá "rt" (aka: real time)
- Ejemplos: rt, daily, monthly

**Secundaria**
- Campo opcional. Sufijo "sec" a incluir cuando la colección contenga datos secundarios

**Ejemplos**
- sonometros_noiselevelobserved_ruzafa_elecnor_daily
- sonometros_noiselevelobserved_bsg. En este ejemplo se han omitido el Proyecto y la Periodicidad
- sonometros_noiselevelobserved_sec. Aquí se guardan los campos secundarios de varias entidades similares
