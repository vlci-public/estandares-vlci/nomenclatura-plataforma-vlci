**Índice**  

 [[_TOC_]]  
  
  
# Nomenclatura. 

## Identificador Entidades
Misma norma que la que se explicar en el apartado: [IoT Agent. Identificador Devices](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/tree/main/iotagents?ref_type=heads#identificador-devices), excepto para las entidades de tipo KeyPerformanceIndicator, que se explica a continuación.


### Entidades KPIs
Los IDs de una entidad KeyPerformanceIndicador deben seguir la nomenclatura: 
* Indicadores de Servicio: [IS] _ [SERVICIO] _ ###
* Indicadores de Ciudad: [IC]  _ ###

Ejemplos de Indicadores de Servicio :

* IS_TIC_055 (Del SERTIC)
* IS_SDA_001 (Del servicio devesa albufera)

Ejemplos de Indicadores de Ciudad :

* IC_001
* IC_032
* IC_125

El campo kpiInternalIdentifier contendrá el mismo valor que el ID de la entidad pero sustituyendo los _ por .

Ejemplos:

| ID | kpiInternalIdentifier |
|----|-----------------------|
| IS_TIC_055 | IS.TIC.055 |
| IC_125 | IC.125 |


## Suscripciones
Todas las suscripciones deben tener el siguiente texto en el campo Descripción: [Equipo] - [Proyecto] - [Destino] - [Tipo_entidad] - [Descripción]
Donde cada uno de los campos entre corchetes tiene un significado que se explica a continuación.
	
**Equipo**
- OTP, para el equipo del proyecto VLCi
- El nombre de la empresa que realice la suscripción
    
**Proyecto**
- VLCI, para suscripciones realizadas por la OTP dentro del proyecto VLCi. Puede indicarse también el subproyecto dentro del proyecto VLCi. Ejemplo:  VLCI Trafico Realtime
- Otros ejemplos: Impulso C7, Connecta

**Destino**
Indica qué componente tecnológico se suscribe a esta suscripción. Ejemplos: 
- ArcGIS GeoEvent
- CEP
- MongoDB
- PG GIS
- PG VLCi
- PG VLCi URBO LastData
- UPV
- CKAN

**Tipo_entidad**
El tipo de entidad Fiware. Se corresponde con el valor del campo "Tipo" de la suscripción, en la sección "Entidades"
	
**Ejemplos de descripciones correctas de suscripciones**
- OTP - VLCI Trafico Realtime - CEP - OffStreetParking - Aparcamientos
- OTP - VLCI Trafico Realtime - MongoDB - TrafficFlowObserved - Intensidad Trafico por Tramo
- ETRA - Impulso VehElec - ASV - ParkingSpot - Plaza de parking de una estación de recarga de vehículos eléctricos

# Buenas prácticas de la plataforma de ciudad

## Tipos de datos en Context Broker

Ver [IoT Agent. Tipos de datos](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/tree/main/iotagents?ref_type=heads#tipos-de-datos).


# Buenas prácticas de la plataforma de ciudad del Ayuntamiento de València

## Atributos de monitorización (para entidades que haya que monitorizar)
Hay una serie de atributos que se usan para monitorizar el sistema y que son de obligado uso en aquellas entidades que midan datos y por tanto sean susceptibles de monitorización:
* maintenanceOwner 
* maintenanceOwnerEmail 
* serviceOwner 
* serviceOwnerEmail  
* operationalStatus: Cada vez que se actualice la entidad, se debe poner a "ok"
* inactive: (Boolean) Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.

## Otros Atributos
* project. El proyecto bajo el que se crean las entidades. Por ejemplo, para el proyecto impulso su valor es impulsoVLCI-red.es
