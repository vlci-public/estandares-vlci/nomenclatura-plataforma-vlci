**Índice**  

 [[_TOC_]]  
  
  
# Restricciones impuestas por el GIS

- Toda suscripción al GIS debe tener relación 1:1 con las capas del GIS. Es decir, no se puede hacer 1 capa a partir de 2 suscripciones. Esto implica que la relación Entidad ContextBroker - Capa es una relación 1:1
- Toda suscripción al GIS debe tener incluido el atributo location para que se pueda ubicar cada elemento en el mapa.
- No enviar atributos del Context Broker cuyo tipo sea StructuredValue. El GIS prefiere atributos tipo Text, con cada valor del StructuredValue separado por comas. Así por ejemplo en lugar de un atributo StructuredValue con el valor: [Caravan_Mains_Socket, CHAdeMO, CCS/SAE, Dual_CHAdeMO], el atributo Text tendría el siguiente valor: "Caravan_Mains_Socket, CHAdeMO, CCS/SAE, Dual_CHAdeMO"