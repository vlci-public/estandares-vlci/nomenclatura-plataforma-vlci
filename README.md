**Índice**  

 [[_TOC_]]  
  
  
# Subservicios y Usuarios
[Aquí se encuentran las buenas prácticas relativas a los aspectos de la administración de la plataforma: subservicios y usuarios](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/admin/README.md)

# IoT Agent
[Aquí se encuentran las buenas prácticas relativas a los elementos del IoT Agent](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/iotagents/README.md)

# Orion Context Broker
[Aquí se encuentran las buenas prácticas relativas a las entidades y suscripciones del Orion Context Broker](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md)

# CEP
[Aquí se encuentran las buenas prácticas relativas a los elementos del CEP](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/cep/README.md)

# Cygnus
[Aquí se encuentran las buenas prácticas relativas a los elementos del Cygnus](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/cygnus/README.md)

# CKAN
[Aquí se encuentran las buenas prácticas relativas a los elementos del CKAN](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/ckan/README.md)

# Mongo
[Aquí se encuentran las buenas prácticas relativas a los elementos del Mongo](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/mongo/README.md)

# GIS Municipal
El GIS no es un componente específico de la plataforma, pero sí que hay una serie de buenas prácticas para comunicarse con él mediante suscripciones del ContextBroker. [Aquí se encuentran las buenas prácticas relativas a toda esa comunicación con el GIS](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/suscripciones-gis/README.md)